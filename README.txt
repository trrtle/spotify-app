The idea is that this app goes to besteveralbums.com and extracts songs
from the best albums from the year(s) the user chooses and
makes a playlist in spotify with those songs.

this app depends on bs4 and spotipy. in the pipfile.lock file you can see wich versions i use.

the pipfile and pipfile.lock are pipenv files.
Pipenv is a python module that lets you create virtual environments.

you can install bs4 and spotipy with pip install or pipenv install

WHAT THE PROGRAM DOES NOW:
It now asks for wich year or years you want the best albums from.
Then it displays thos albums and artists in the console

FUTURE PLANS:
use spotipy to make a playlist of the albums.
