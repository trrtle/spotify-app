import textwrap, re


class Interaction():
    """ This class is made for all the interaction with the user
    dependencies: textwrap"""

    def opening(self):
        """start of the app"""

        print(textwrap.dedent("""
        Hello, this is the spotify app
        I create a playlist in spotify from the best albums in your favorite genre based on its rating from rateyourmusic.com.
        """))

        input("Press a key to continue...")


    def choose_year(self):
        print(textwrap.dedent("""
        from what year(s) do you want the playlist?
        201 = all 2010s
        200 = all 2000s
        199 = all 1990s
        198 = all 1980s
        and so on...
        you can als type in an individual year.
        if you do not want a specific year just press enter
        """))

        self.year = input("Year: ")


    def d_url_regex(self):
        self.d_url = 0

        d_url_dict  = {
            "201" : re.compile(r"(201)\d*"),
            "200" : re.compile(r"(200)\d*"),
            "199" : re.compile(r"(199)\d*"),
            "198" : re.compile(r"(198)\d*"),
            "197" : re.compile(r"(197)\d*"),
            "196" : re.compile(r"(196)\d*"),
            "195" : re.compile(r"(195)\d*"),
            "194" : re.compile(r"(194)\d*")
        }

        for key, d_regex in d_url_dict.items():
            if d_regex.match(self.year):
                self.d_url = key




