import urllib.request
from bs4 import BeautifulSoup
import re

class Web():
    """url as argument required
    contains all methods related to web scraping"""


    def __init__(self, url):
        self.src_code = urllib.request.urlopen(url)


    def crawl(self):
        """crawls the url and returns a dictionary called album_dict
        with album name as key and artists as value"""

        album_artist_list = []
        self.album_dict = {}

        bs_object = BeautifulSoup(self.src_code, 'html.parser')

        album_titles = bs_object.findAll("a",{"class" : "nav2emph"})

        for title in album_titles:
            album_artist_list.append(str(title.text))

        n = 0
        for i in range(len(album_artist_list)- 1):
            self.album_dict[album_artist_list[n]] = album_artist_list[n + 1]
            n = n + 2
            if n >= len(album_artist_list):
                break
            else:
                continue

        return self.album_dict

