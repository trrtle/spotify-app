from dependencies.interaction import Interaction
from dependencies.webcrawl import Web

main_loop = Interaction

main_loop.opening(main_loop)
main_loop.choose_year(main_loop)
main_loop.d_url_regex(main_loop)

website_uno = Web(f"https://www.besteveralbums.com/topratedstats.php?o=album&ur=0&r=10&cp=1&l=0&d={main_loop.d_url}&y={main_loop.year}")
Web.crawl(website_uno)
print(website_uno.album_dict)
